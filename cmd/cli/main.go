package main

import "gitlab.com/bulka/internal/app"

func main() {
	a := app.New()
	a.Run()
}
