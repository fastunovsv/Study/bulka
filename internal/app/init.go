package app

import (
	"context"
	"log/slog"
	"os"

	"gitlab.com/bulka/internal/storage"
)

const (
	StartBucketSize int = 10_000
)

type Application struct {
	ctx context.Context
	s   *storage.Storage
}

func New() *Application {
	return &Application{
		ctx: context.Background(),
		s:   storage.New(StartBucketSize),
	}
}

func (a *Application) Init() {

	opts := &slog.HandlerOptions{
		Level: slog.LevelDebug,
	}
	slog.SetDefault(slog.New(slog.NewJSONHandler(os.Stdout, opts)))

	slog.Info("application initialization successful")

}
