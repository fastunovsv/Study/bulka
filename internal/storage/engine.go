package storage

import "context"

type Storage struct {
	Bucket map[string]string
}

func New(capacity int) *Storage {
	return &Storage{
		Bucket: make(map[string]string, capacity),
	}
}

func (s *Storage) Set(_ context.Context, key, value string) error {
	s.Bucket[key] = value
	return nil
}

func (s *Storage) Get(_ context.Context, key string) (string, error) {
	response, ok := s.Bucket[key]
	if !ok {
		return "", ErrNoFound
	}
	return response, nil
}

func (s *Storage) Del(_ context.Context, key string) error {
	delete(s.Bucket, key)
	return nil
}
