package storage

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
)

const (
	TestBucketSize int    = 10
	TestKey        string = "TestKey"
	TestValue      string = "TestValue"
)

func TestSet(t *testing.T) {
	storage := New(TestBucketSize)
	err := storage.Set(context.Background(), TestKey, TestValue)

	assert.Nil(t, err)
	assert.Equal(t, TestValue, storage.Bucket[TestKey])
}

func TestGet(t *testing.T) {
	storage := New(TestBucketSize)
	storage.Bucket[TestKey] = TestValue

	exp, err := storage.Get(context.Background(), TestKey)

	assert.Nil(t, err)
	assert.NotNil(t, exp)
	assert.Equal(t, exp, storage.Bucket[TestKey])
}

func TestDel(t *testing.T) {
	storage := New(TestBucketSize)
	storage.Bucket[TestKey] = TestValue

	err := storage.Del(context.Background(), TestKey)

	assert.Nil(t, err)
	assert.NotEqual(t, TestValue, storage.Bucket[TestKey])
	assert.Equal(t, "", storage.Bucket[TestKey])
}
