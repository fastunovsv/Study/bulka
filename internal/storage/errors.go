package storage

import "errors"

var ErrNoFound = errors.New("no value for requested key")
